function [ Counter ] = CombineCurves( Cx_small, Cy_small, Cx_counter)
%Takes a list of unique bezier function descriptors and combines them so
%that similar curves get better numbers. 

Category = zeros(1,length(Cx_small)); 
ComplexC = Cx_small + sqrt(-1)*Cy_small; 
iterator = 1; 
Counter = zeros(1,length(Cx_small)); 

CCUpsideDown = [ComplexC(4,:); ComplexC(3,:); ComplexC(2,:); ComplexC(1,:)];

while ~isempty(find(Category==0,1)); 
    Current = ComplexC(:,iterator); 
    if Category(iterator) == 0 
        Difference = bsxfun(@minus, ComplexC, Current);
        DUpsideDown = bsxfun(@minus, CCUpsideDown, Current);
        Metric = sum ( abs(Difference), 1); 
        MUpsideDown = sum ( abs(DUpsideDown), 1); 
        Category(Metric<35) = max(Category) +1; 
        Category(MUpsideDown<20) = max(Category); 
        Counter(iterator) = sum(Cx_counter(Category == max(Category))); 
    end 
    iterator = iterator +1; 
end 
    


