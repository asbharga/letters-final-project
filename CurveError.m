function [ error ] = CurveError( x,y,xreal,yreal )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
complex = x+sqrt(-1)*y; 
complex_real = xreal+sqrt(-1)*yreal; 
[l,b] = size(x); 
[~,a] = size(xreal);

difference = zeros(b,a); 
for i =1:b
    for j = 1:a
        m = complex(:,i);
        n = complex_real(:,j);
        
        for k=1:length(m)
            difference(i,j) = difference(i,j) + min(abs(n-m(k))); 
        end
    end 
end 
difference= difference/l;

error = sum(sum(difference)); 

end

