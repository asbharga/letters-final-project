function [ xsub, ysub, xnotsub, ynotsub ] = FindLineSeg( xsub, ysub, xnotsub, ynotsub )
% Takes in a set of points, separated into two groups
% xsub, ysub are points within the line segment
% xnotsub, ynotsub are available points to be added to the line segment
    
    % Run: 
    % compute the linear regression between the last two points added
    % find the point which is closest and in the correct direction and has the
    % smallest residual 
    % add that point to the set
    % keep running until there are no close-and-in-the correct-direction points

    % Keep the vector of those points and subtract them from the total set
    % Start with the closest point to the end of the set which was excluded
    % from the set because of wrong-angle-nearness
    % perform the algorithm with the new set of points 

    done = 0; 
    k = length(xsub); 
    while done ==0; 
        k = length(xsub)+1; 

        % calculate metric for adding points 
        if k>2 && (xsub(k-2) ~= xsub(k-1)) 
            p = polyfit(xsub(k-2:k-1),ysub(k-2:k-1), 1); 
            y = polyval(p,xnotsub); 
            %res_out = abs(ynotsub-y);
            Q2 = [xnotsub(1); y(1)];
            Q1 = [xnotsub(length(xnotsub-1)); y(length(xnotsub-1))];
            V = [xnotsub; ynotsub]; 
            if abs(p(1)) >10^5 
                res_out = xnotsub - xsub(k-1); 
            else 
                %res_out = abs(ynotsub-y); 
                res_out = []; 
                for i = 1:length(xnotsub)
                    res_out(i) = abs(det([Q2-Q1,V(:,i)-Q1]))/abs(Q2(1)-Q1(1));
                end 
            end 
        else
            res_out = zeros(1,length(xnotsub)); 
        end 
        distance = (xsub(k-1)-xnotsub).^2 + (ysub(k-1)-ynotsub).^2;
        

        if k>2
            distance2 = (xsub(k-2)-xnotsub).^2 + (ysub(k-2)-ynotsub).^2;
            qualifies = distance - distance2; 
            qualifies = sign(qualifies);
            qualifies(qualifies<0) = 0; 
            
            newp2x = 2*xsub(k-1) - xsub(k-2);
            newp2y = 2*ysub(k-1) - ysub(k-2);
            myangle = ComputeAngle(xsub(k-1),ysub(k-1),newp2x, newp2y ,xnotsub,ynotsub); 
            qualifies(myangle> pi/2 ) = 1; 
        else
            qualifies = zeros(1,length(xnotsub)); 
            myangle = zeros(1,length(xnotsub)); 
        end 
        
         for i =1:length(res_out) 
            if res_out(i)> 40
                qualifies(i) = 1; 
            end 
         end 

        if sum(qualifies)/length(qualifies) == 1 && k > 5 
            done = 1; 
        end 
        
        if min(distance) > 60 
            done =1; 
        end 
        
        if length(xnotsub) ==1 
            done = 1;
        end 

        %choose best point
        [~, n] = min(res_out + distance*.4 + qualifies *1000000 + 4.^(myangle));
        
        if distance(n) > 100
            done=1;
        end 
        
        if myangle(n) > pi/2.2
            done=1;
        end 
        
        if distance(n) ~=0
            xsub = [xsub, xnotsub(n)];
            ysub = [ysub, ynotsub(n)];
        end 
        ynotsub(n) = [];
        xnotsub(n) = [];

        %Plot stuff
%          if done > 0 
%             figure(10) 
%             plot(xsub,ysub, 'g*');
%             hold on 

%             if k>3
%                 x_plot = min(xsub(k-2:k)):max(ysub(k-2:k)); 
%                 y_plot = polyval(p,x_plot);
%                 plot(x_plot,y_plot, 'r'); 
%             end 
%              plot(xnotsub, ynotsub, '*')
%         end 

    end 

end

function [angle] = ComputeAngle(P1x, P1y, P2x, P2y, P3x, P3y) 
    a = sqrt( (P2x-P3x).^2 + (P2y-P3y).^2 ); 
    b = sqrt( (P2x-P1x)^2 + (P2y-P1y)^2 ); 
    c = sqrt( (P1x-P3x).^2 + (P1y-P3y).^2 ); 
    
    angle = abs(acos( (b.^2 + c.^2 -a.^2)./(2*b*c) )); 

end 

