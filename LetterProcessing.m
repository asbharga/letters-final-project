function [ xind_out, yind_out ] = LetterProcessing( A )
%preprocessing the image 

A = A+1; 
A(:, find(sum(abs(A)) == 0)) = []; 
k=1; 
val = max(min(A));

[h, w] = size(A); 

for j = 1:w 
    for i = 1:h 
        if A(i,j) < val*.8
            A(i,j) =60; 
            xind(k) = j; 
            yind(k) = i;
            k = k+1; 
        else 
            A(i,j) = 0; 
        end 
    end 
end 


nsame = 0; 
xind_out = [];
yind_out = [];

for j = min(xind): max(xind); 
    y_temp = yind(xind==j); 
    nsame = 0; 
    y_curr = y_temp(1); 
    if length(y_temp) ==1 
        xind_out(length(xind_out)+1) = j; 
        yind_out(length(xind_out)) = y_curr; 
    else 
        for i = 1:length(y_temp)+1
            if i < length(y_temp)+1
                y_next = y_temp(i); 
            end 

            if (y_next-y_curr)> 1 || i==length(y_temp)+1 || nsame==2
                xind_out(length(xind_out)+1) = j; 
                if nsame > 0
                    yind_out(length(xind_out)) = sum(y_temp(i-nsame:i-1))/nsame; 
                else 
                    yind_out(length(xind_out)) = y_temp(i-1);     
                end 
                nsame = 0; 
            else 
                nsame = nsame +1; 
            end 
            y_curr = y_next; 

        end 
    end 

end


yind_out = max(yind_out) - yind_out;
yind = max(yind) - yind;
figure
plot(xind,yind,'*'); 
figure 
plot(xind_out,yind_out,'r*'); 

end 
