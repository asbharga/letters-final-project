PrintWriter output;
PrintWriter output2; 

void setup() {
  size(40, 120); 
  noFill();
  output = createWriter("x.txt"); 
  output2 = createWriter("xoutput.txt"); 

//}
  int w = 40; 
//void draw() {
  background(255);
  stroke(1,0,0); 

  int mid = 40;
  int top = 5;
  int base = 80;
  int bottom = 118; 
  int var = 0; 
  int xlength = 1200; 
  /*line(0,base, xlength,base); 
  line(0,mid, xlength,mid); 
  line(0,top, xlength,top); 
  line(0,bottom, xlength,bottom); */ 
  
  stroke(0);
  

  int[] s = {0,0,0,0}; 
  s = write_x(5,base,10,0, mid, top, base,bottom, var);


  loadPixels(); 
  for (int j=0; j<120; j++) {
      for (int i=0; i<w; i++) {
      color b = pixels[i+j*w];
      if (i== w-1) {
        output.println(b);
      }
      else {
        output.print(b);
        output.print(','); 
      }
    }
  }
  output.close();  // Finishes the file  
  
  for (int j=4; j<s.length; j++) {
    output2.println(s[j]); 
  }
  output2.close(); 


}


 int[] write_a(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 30 + int(random(-var,var));
  int mid1y = mid+ int(random(-var,var)); 
  int mid2x = mid1x+ int(random(-var,var)); 
  int mid2y = mid1y+ int(random(-var,var)); 
  int endx = startx + 40+ int(random(-var,var));
  int endy = base - int(.3*(base-mid)) + int(random(-var,var)); 
  
  int control1x = startx + sx;
  int control1y = starty+sy;
  
  int control2x = mid1x - 15+ int(random(-var,var));
  int control2y = mid1y + 5+ int(random(-var,var)); 
  int control3x = mid1x - 50+ int(random(-var,var));
  int control3y = mid1y + int(random(0,2*var));
  int control4x = mid1x - 30 + int(random(-var,var));
  int control4y = mid1y + int(2.2*(base-mid1y));
  
  int control5x = mid2x - 20+ int(random(-var,var));
  int control5y = mid2y + int(1.6*(base-mid1y))+ int(random(-var,var)); 
  int control6x = endx - 5+ int(random(-var,var));
  int control6y = endy + 6+ int(random(-var,var)); 
  
  
  s[0] = endx - control6x;
  s[1] = endy - control6y; 
  s[2] = endx; 
  s[3] = endy; 
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, endx, endy};  
  s = concat(s, cArray); 
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, endx, endy);
  endShape();
  
  return s; 
}

int[] write_b(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 20 + int(random(-var,var));
  int mid1y = top+ int(random(-var,var)); 
  int mid2x = mid1x + int(random(-var,var));
  int mid2y = base+ int(random(-var,var));
  int mid3x = int(.5*(mid1x+startx)) + 5 + int(random(-var,var)); 
  int mid3y = mid+ int(random(-var,var)); 
  int endx = startx + 45+ int(random(-var,var));
  int endy = mid + int(.3*(base-mid))+ int(random(-var,var)); 
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x + 6;
  int control2y = mid1y - 4 ; 
  
  int control3x = mid1x -6;
  int control3y = mid1y +4;
  int control4x = mid2x -30;
  int control4y = mid2y - 10; 

  int control5x = mid2x + 15;
  int control5y = mid2y + 5;  
  int control6x = mid3x +40;
  int control6y = mid3y +5; 
  
  int control7x = mid3x -8;
  int control7y = mid3y +8;   
  int control8x = endx -5; 
  int control8y = endy;
  
  s[0] = endx - control8x;
  s[1] = endy - control8y; 
  s[2] = endx; 
  s[3] = endy; 
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, mid3x, mid3y);
  bezierVertex(control7x, control7y, control8x, control8y, endx, endy);
  endShape();
  
  return s; 
}


 int[] write_c(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int midx = startx +28+ int(random(-var,var));
  int midy = mid + 7 + int(random(-var,var)); 
  int mid2x = midx+ int(random(-var,var));
  int mid2y = base -10+ int(random(-var,var));
  int endx = startx + 60+ int(random(-var,var));
  int endy = base - int(.3*(base-mid))+ int(random(-var,var)); 
  
  int control1x = startx + sx;
  int control1y = starty+sy;
  int control2x = midx - (mid2y-mid)/2;
  int control2y = mid -10; 
  
  int control3x = midx -30; 
  int control3y = midy -50; 
  int control4x = mid2x - 50;
  int control4y = base - 3*(mid2y-base);
  
  int control5x = mid2x +6;
  int control5y = mid2y ; 
  int control6x = endx - 20;
  int control6y = endy + 20; 
  
  s[0] = mid2x - control4x;
  s[1] = mid2y - control4y; 
  s[2] = mid2x; 
  s[3] = mid2y; 
  
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, midx, midy, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, endx, endy};  
  s = concat(s, cArray); 
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, midx, midy);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  //bezierVertex(control5x, control5y, control6x, control6y, endx, endy);
  endShape();
  
  return s; 
}

 int[] write_d(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 45+ int(random(-var,var));
  int mid1y = mid+ int(random(-var,var)); 
  int mid2x = mid1x + 10+ int(random(-var,var)); 
  int mid2y = top+ int(random(-var,var)); 
  int endx = startx + 60+ int(random(-var,var));
  int endy = base+ int(random(-var,var)); 
  
  int control1x = startx + sx;
  int control1y = starty+sy;
  int control2x = mid1x - 10;
  int control2y = mid1y; 
  
  int control3x = mid1x - 80;
  int control3y = mid1y - int(.3*(base-mid1y)) ;
  int control4x = mid1x;
  int control4y = mid1y + int(.7*(base-top)) + int(1.7*(base-mid));
  
  int control5x = mid2x - 20;
  int control5y = mid2y + 70; 
  int control6x = endx - 20;
  int control6y = endy; 
  
  s[0] = endx - control6x;
  s[1] = endy - control6y; 
  s[2] = endx; 
  s[3] = endy; 
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, endx, endy);
  endShape();
  
  return s; 
}
 int[] write_e(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
  int[] s = {0,0,0,0};
  
  int ystartpos = int(.2*(base-starty));
  
  int mid1x = startx + 20 +ystartpos+ int(random(-var,var));
  int mid1y = mid + ystartpos +4 + int(random(-var,var)); 
  int mid2x = mid1x+ int(random(-var,var));
  int mid2y = base+ int(random(-var,var));
  int endx = startx + 45 + int(random(-var,var));
  int endy = base -10+ int(random(-var,var)); 
  
  int control1x = startx+sx;
  int control1y = starty+sy; 
  int control2x = mid1x + ystartpos;
  int control2y = mid1y + ystartpos +20; 
  
  int control3x = mid1x - ystartpos;
  int control3y = mid1y - ystartpos -20;
  int control4x = mid2x -20;
  int control4y = mid2y; 
  
  int control5x = mid2x + 5;
  int control5y = mid2y; 
  int control6x = endx - 8;
  int control6y = endy + 8; 

  s[0] = endx-control6x; 
  s[1] = endy-control6y; 
  s[2] = endx; 
  s[3] = endy; 
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, endx, endy);
  endShape();
  
  return s; 
 }

int[] write_f(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 25 + int(random(-var,var));
  int mid1y = top+ int(random(-var,var)); 
  int mid2x = mid1x + int(random(-var,var));
  int mid2y = bottom  + int(random(-var,var));
  int mid3x = mid1x-10+ int(random(-var,var)); 
  int mid3y = mid+ 20+ int(random(-var,var)); 
  int endx = startx + 50+ int(random(-var,var));
  int endy = starty+ int(random(-var,var)); 
  
  int control1x = startx+sx;
  int control1y = starty+sy; 
  int control2x = mid1x + 6;
  int control2y = mid1y - 4 ; 
  
  int control3x = mid1x -6;
  int control3y = mid1y +4;
  int control4x = mid2x -30;
  int control4y = mid2y - 10; 

  int control5x = mid2x + 15;
  int control5y = mid2y + 5;  
  int control6x = mid3x +40;
  int control6y = mid3y +5; 
  
  int control7x = mid3x -8;
  int control7y = mid3y -1;   
  int control8x = endx -20; 
  int control8y = endy +10;
  
  s[0] = endx - control8x;
  s[1] = endy - control8y; 
  s[2] = endx; 
  s[3] = endy; 
  
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, mid3x,mid3y, control7x, control7y, control8x, control8y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, mid3x, mid3y);
  bezierVertex(control7x, control7y, control8x, control8y, endx, endy);
  endShape();
  
  return s; 
}


int[] write_g(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 40+ int(random(-var,var));
  int mid1y = mid+ int(random(-var,var)); 
  int mid2x = mid1x+ int(random(-var,var)); 
  int mid2y = mid1y+ int(random(-var,var)); 
  int mid3x = mid2x - 25+ int(random(-var,var)); 
  int mid3y = bottom+ int(random(-var,var)); 
  int endx = startx + 55+ int(random(-var,var));
  int endy = starty+ int(random(-var,var)); 
  
  int control1x = startx + sx;
  int control1y = starty+sy;
  
  int control2x = mid1x - 10;
  int control2y = mid1y; 
  int control3x = mid1x - 80;
  int control3y = mid1y;
  int control4x = mid1x - 30;
  int control4y = mid1y + int(2.2*(base-mid1y));
  
  int control5x = mid2x;
  int control5y = mid2y + 80; 
  int control6x = mid3x + 4;
  int control6y = mid3y; 
 
  int control7x = mid3x -10;
  int control7y = mid3y; 
  int control8x = endx -30;
  int control8y = endy + 30; 
  
  s[0] = endx - control8x;
  s[1] = endy - control8y;
  s[2] = endx; 
  s[3] = endy; 
  
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, mid3x,mid3y, control7x, control7y, control8x, control8y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, mid3x, mid3y);
  bezierVertex(control7x, control7y, control8x, control8y, endx, endy);
  endShape();
  
  return s; 
}


int[] write_h(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 20+ int(random(-var,var));
  int mid1y = top+ int(random(-var,var)); 
  int mid2x = mid1x - 10+ int(random(-var,var));
  int mid2y = base+ int(random(-var,var));
  int mid3x = mid2x + 15+ int(random(-var,var)); 
  int mid3y = mid+ int(random(-var,var)); 
  int endx = startx + 50+ int(random(-var,var));
  int endy = base+ int(random(-var,var)); 
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x + 10;
  int control2y = mid1y - int(.2*(base-mid)) ; 
  
  int control3x = mid1x -10;
  int control3y = mid1y + int(.2*(base-mid));
  int control4x = mid2x;
  int control4y = mid2y - 10; 

  int control5x = mid2x;
  int control5y = mid2y - (base-mid);  
  int control6x = mid3x -8;
  int control6y = mid3y; 
  
  int control7x = mid3x +8;
  int control7y = mid3y;   
  int control8x = endx -20; 
  int control8y = endy +10;
  
  s[0] = endx - control8x;
  s[1] = endy - control8y; 
  s[2] = endx; 
  s[3] = endy; 
  
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, mid3x,mid3y, control7x, control7y, control8x, control8y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, mid3x, mid3y);
  bezierVertex(control7x, control7y, control8x, control8y, endx, endy);
  endShape();
  
  return s; 
}


int[] write_i(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 15 + int(random(-var,var));
  int mid1y = mid + int(random(-var,var)); 
  int endx = mid1x + (mid1x-startx) + int(random(-var,var));
  int endy = base + int(random(-var,var)); 

  int control1x = startx+sx;
  int control1y = starty+sy; 
  int control2x = mid1x +4;
  int control2y = mid1y - 4 ; 
  
  int control3x = mid1x -4;
  int control3y = mid1y +4;
  int control4x = endx -20;
  int control4y = endy; 

  s[0] = endx - control4x;
  s[1] = endy - control4y;
  s[2] = endx; 
  s[3] = endy; 
  
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, endx, endy);
  endShape();
  
  return s; 
}

int[] write_j(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 20 + int(random(-var,var));
  int mid1y = mid + int(random(-var,var)); 
  int mid2x = mid1x + int(random(-var,var));
  int mid2y = bottom+ int(random(-var,var));
  int mid3x = startx + 20 + int(random(-var,var)); 
  int mid3y = base -5 + int(random(-var,var)); 
  int endx = startx + 50;
  int endy = base -20 + int(random(-var,var)); 
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x + 6;
  int control2y = mid1y - 4 ; 
  
  int control3x = mid1x -6;
  int control3y = mid1y +4;
  int control4x = mid2x +30;
  int control4y = mid2y - 10; 

  int control5x = mid2x - 30;
  int control5y = mid2y + 10;  
  int control6x = mid3x -10;
  int control6y = mid3y; 
  
  int control7x = mid3x +10;
  int control7y = mid3y;   
  int control8x = endx -20; 
  int control8y = endy +10;
  
  s[0] = endx - control8x;
  s[1] = endy - control8y;
  s[2] = endx; 
  s[3] = endy; 
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, mid3x,mid3y, control7x, control7y, control8x, control8y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, mid3x, mid3y);
  bezierVertex(control7x, control7y, control8x, control8y, endx, endy);
  endShape();
  
  return s; 
}


int[] write_k(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 10 + int(random(-var,var));
  int mid1y = top+ int(random(-var,var)); 
  int mid2x = mid1x -  10 + int(random(-var,var));
  int mid2y = base+ int(random(-var,var));
  int mid3x = mid2x + int(random(-var,var)); 
  int mid3y = mid + int(.3*(base-mid)) + int(random(-var,var)); 
  int endx = startx + 30+ int(random(-var,var));
  int endy = base+ int(random(-var,var)); 
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x + 6;
  int control2y = mid1y - 4 ; 
  
  int control3x = mid1x -6;
  int control3y = mid1y +4;
  int control4x = mid2x;
  int control4y = mid2y - 10; 

  int control5x = mid2x;
  int control5y = mid2y - 75;  
  int control6x = mid3x +60;
  int control6y = mid3y +15; 
  
  int control7x = mid3x +2;
  int control7y = mid3y +32;   
  int control8x = endx -8; 
  int control8y = endy;
  
  s[0] = endx - control8x;
  s[1] = endy - control8y; 
  s[2] = endx; 
  s[3] = endy; 
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, mid3x,mid3y, control7x, control7y, control8x, control8y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, mid3x, mid3y);
  bezierVertex(control7x, control7y, control8x, control8y, endx, endy);
  endShape();
  
  return s; 
}

int[] write_l(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 35;
  int mid1y = top; 
  int endx = mid1x;
  int endy = base; 
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x +15;
  int control2y = mid1y; 
  
  int control3x = mid1x-10;
  int control3y = mid1y;
  int control4x = endx -20;
  int control4y = endy; 

  s[0] = endx - control4x;
  s[1] = endy - control4y; 
  s[2] = endx; 
  s[3] = endy; 
  
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, endx, endy);
  endShape();
  
  return s; 
}

int[] write_m(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 10;
  int mid1y = mid; 
  int mid2x = mid1x;
  int mid2y = base;
  int mid3x = mid2x + 20; 
  int mid3y = base; 
  int mid4x = mid3x +20; 
  int mid4y = base; 
  int endx = mid4x +10; 
  int endy = base -5; 
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x;
  int control2y = mid1y + 10 ; 
  
  int control3x = mid1x;
  int control3y = mid1y +10;
  int control4x = mid2x;
  int control4y = mid2y - 10; 

  //first lump 
  int control5x = mid2x + 8;
  int control5y = mid2y - int(1.2*(base-mid1y));  
  int control6x = mid3x;
  int control6y = mid3y - int(1.2*(base-mid1y)); 
  
  int control7x = mid3x + 8;
  int control7y = mid3y - int(1.2*(base-mid1y));    
  int control8x = mid4x; 
  int control8y = mid4y- int(1.2*(base-mid1y)); 
  
  int control9x = mid4x +4;
  int control9y = mid4y +6;   
  int control10x = endx -5; 
  int control10y = endy +5;
  
  s[0] = endx - control10x;
  s[1] = endy - control10y; 
  s[2] = endx; 
  s[3] = endy; 
  
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, mid3x, mid3y, control7x, control7y, control8x, control8y, mid4x, mid4y ,control9x, control9y, control10x, control10y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, mid3x, mid3y);
  bezierVertex(control7x, control7y, control8x, control8y, mid4x, mid4y);
  bezierVertex(control9x, control9y, control10x, control10y, endx, endy);
  endShape();
  
  return s; 
}


int[] write_n(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 15;
  int mid1y = mid; 
  int mid2x = mid1x;
  int mid2y = base;
  int mid3x = mid2x + 25; 
  int mid3y = base -8; 
  int endx = startx + 50;
  int endy = base -4; 
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x;
  int control2y = mid1y + 10 ; 
  
  int control3x = mid1x;
  int control3y = mid1y +10;
  int control4x = mid2x;
  int control4y = mid2y - 10; 

  int control5x = mid2x;
  int control5y = mid2y - int(1.2*(base-mid1y));  
  int control6x = mid3x -8;
  int control6y = mid3y -int(1.2*(base-mid1y)); 
  
  int control7x = mid3x +1;
  int control7y = mid3y +int(.2*(base-mid1y));   
  int control8x = endx -3; 
  int control8y = endy+8;
  
  s[0] = endx - control8x;
  s[1] = endy - control8y;
  s[2] = endx; 
  s[3] = endy; 
  
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, mid3x,mid3y, control7x, control7y, control8x, control8y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, mid3x, mid3y);
  bezierVertex(control7x, control7y, control8x, control8y, endx, endy);
  endShape();
  
  return s; 
}



 int[] write_o(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
  int[] s = {0,0,0,0};
  int mid1x = startx + 12;
  int mid1y = mid; 
  int mid2x = startx +15;
  int mid2y = base;
  int mid3x = mid2x; 
  int mid3y = mid; 
  int endx = startx + 35;
  int endy = mid + int(.1*(base-mid));   
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x;
  int control2y = mid1y - 6 ; 
  
  int control3x = mid1x -10;
  int control3y = mid1y + 6;
  int control4x = mid2x -20;
  int control4y = mid2y; 
  
  int control5x = mid2x +25;
  int control5y = mid2y; 
  int control6x = mid3x +20;
  int control6y = mid3y; 
  
  int control7x = mid3x ;
  int control7y = mid3y;   
  int control8x = endx -10; 
  int control8y = endy; 

  s[0] = endx - control8x;
  s[1] = endy - control8y;
  s[2] = endx; 
  s[3] = endy; 
  
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, mid3x,mid3y, control7x, control7y, control8x, control8y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, mid3x, mid3y);
  bezierVertex(control7x, control7y, control8x, control8y, endx, endy);
  endShape();
  
  return s; 
}


int[] write_p(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 15;
  int mid1y = mid; 
  int mid2x = mid1x - 3;
  int mid2y = bottom;
  int mid3x = mid1x + 15; 
  int mid3y = mid1y; 
  int mid4x = startx + 10; 
  int mid4y = base; 
  int endx = startx + 50;
  int endy = base -10; 
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x -5;
  int control2y = mid1y + 5 ; 
  
  int control3x = mid1x;
  int control3y = mid1y +10;
  int control4x = mid2x;
  int control4y = mid2y - 10; 

  int control5x = mid2x;
  int control5y = mid2y;  
  int control6x = mid3x -20;
  int control6y = mid3y; 
  
  int control7x = mid3x +20;
  int control7y = mid3y;   
  int control8x = mid4x+ 20; 
  int control8y = mid4y + 10;
  
  int control9x = mid4x;
  int control9y = mid4y +10;
  int control10x = endx- 8; 
  int control10y = endy + 4;
  
  s[0] = endx - control10x;
  s[1] = endy - control10y;
  s[2] = endx; 
  s[3] = endy; 
  
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, mid3x, mid3y, control7x, control7y, control8x, control8y, mid4x, mid4y ,control9x, control9y, control10x, control10y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, mid3x, mid3y);
  bezierVertex(control7x, control7y, control8x, control8y, mid4x, mid4y);
  bezierVertex(control9x, control9y, control10x, control10y, endx, endy);
  endShape();
  
  return s; 
}

 int[] write_q(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int midx = startx +35;
  int midy = mid; 
  int mid2x = midx;
  int mid2y = midy;
  int mid3x = midx -10;
  int mid3y = bottom;
  int mid4x = mid3x; 
  int mid4y = base -10; 
  int endx = startx + 45;
  int endy = base; 
  
  int control1x = startx + sx;
  int control1y = starty+sy;
  int control2x = midx -20;
  int control2y = midy; 
  
  int control3x = midx -80; 
  int control3y = midy; 
  int control4x = mid2x;
  int control4y = mid2y + int(2.2*(base-mid)); ;
  
  int control5x = mid2x -5;
  int control5y = mid2y ; 
  int control6x = mid3x -10;
  int control6y = mid3y; 
  
  int control7x = mid3x +6;
  int control7y = mid3y;   
  int control8x = mid4x +4; 
  int control8y = mid4y;
  
  int control9x = mid4x +10;
  int control9y = mid4y +15;
  int control10x = endx - 1; 
  int control10y = endy;

  
  s[0] = endx - control10x;
  s[1] = endy - control10y;
  s[2] = endx; 
  s[3] = endy; 
  
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, midx, midy, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, mid3x, mid3y, control7x, control7y, control8x, control8y, mid4x, mid4y ,control9x, control9y, control10x, control10y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, midx, midy);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, mid3x, mid3y);
  bezierVertex(control7x, control7y, control8x, control8y, mid4x, mid4y);
  bezierVertex(control9x, control9y, control10x, control10y, endx, endy);
  endShape();
  
  return s; 
}

 int[] write_r(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 10;
  int mid1y = mid; 
  int mid2x = mid1x + 15;
  int mid2y = mid1y +10;
  int endx = startx + 38;
  int endy = base; 
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x+4;
  int control2y = mid1y + 16; 
  
  int control3x = mid1x;
  int control3y = mid1y+1;
  int control4x = mid2x;
  int control4y = mid2y; 

  int control5x = mid2x - 15;
  int control5y = mid2y + 25; 
  int control6x = endx - 5;
  int control6y = endy +1; 
  
  s[0] = endx - control6x;
  s[1] = endy - control6y; 
  s[2] = endx; 
  s[3] = endy; 
  
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, endx, endy};  
  s = concat(s, cArray);
  
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, endx, endy);
  endShape();
  
  return s; 
}


 int[] write_s(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 30;
  int mid1y = mid; 
  int mid2x = int(.5*(startx+mid1x));
  int mid2y = base - int(.2*(base-mid)); 
  int endx = startx + 45;
  int endy = base; 
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x+4;
  int control2y = mid1y + 8; 
  
  int control3x = mid1x + int(.5*(endx-mid2x));
  int control3y = mid1y+ int(.4*(base-mid));
  int control4x = mid2x + (endx-mid2x);
  int control4y = mid2y +int(.5*(base-mid)); 

  int control5x = mid2x +10;
  int control5y = mid2y + 10; 
  int control6x = endx - 5;
  int control6y = endy + 5; 
  
  s[0] = endx - control6x;
  s[1] = endy - control6y; 
  s[2] = endx; 
  s[3] = endy; 
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, endx, endy);
  endShape();
  
  return s; 
}

int[] write_t(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 35;
  int mid1y = top; 
  int endx = mid1x;
  int endy = base; 
  
  int cross1x = mid1x-20;
  int cross1y = mid;
  int cross2x = mid1x+7; 
  int cross2y = mid; 
  line(cross1x, cross1y, cross2x, cross2y); 
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x +15;
  int control2y = mid1y; 
  
  int control3x = mid1x-10;
  int control3y = mid1y;
  int control4x = endx -20;
  int control4y = endy; 

  s[0] = endx - control4x;
  s[1] = endy - control4y; 
  s[2] = endx; 
  s[3] = endy; 
  
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, endx, endy};  
  s = concat(s, cArray);
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, endx, endy);
  endShape();
  
  return s; 
}

int[] write_u(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 10;
  int mid1y = mid; 
  int mid2x = mid1x +12;
  int mid2y = base;
  int mid3x = mid2x +12; 
  int mid3y = mid; 
  int endx = mid3x +10;
  int endy = base; 
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x -5;
  int control2y = mid1y + 5 ; 
  
  int control3x = mid1x;
  int control3y = mid1y +10;
  int control4x = mid2x -15;
  int control4y = mid2y ; 

  int control5x = mid2x +15;
  int control5y = mid2y;  
  int control6x = mid3x;
  int control6y = mid3y +10 ; 
  
  int control7x = mid3x +4;
  int control7y = mid3y +12;   
  int control8x = endx -10; 
  int control8y = endy +10;
  
  s[0] = endx - control8x;
  s[1] = endy - control8y;
  s[2] = endx; 
  s[3] = endy; 
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, mid3x,mid3y, control7x, control7y, control8x, control8y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, mid3x, mid3y);
  bezierVertex(control7x, control7y, control8x, control8y, endx, endy);
  endShape();
  
  return s; 
}

int[] write_v(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 15;
  int mid1y = mid; 
  int mid2x = mid1x +10;
  int mid2y = base;
  int endx = mid2x +12; 
  int endy = mid; 

  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x-5;
  int control2y = mid1y; 
  
  int control3x = mid1x+5;
  int control3y = mid1y;
  int control4x = mid2x +3;
  int control4y = mid2y; 

  int control5x = mid2x +15;
  int control5y = mid2y;    
  int control6x = endx; 
  int control6y = endy;
  
  s[0] = endx - control6x;
  s[1] = endy - control6y;
  s[2] = endx; 
  s[3] = endy; 
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, endx, endy};  
  s = concat(s, cArray);
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, endx, endy);
  endShape();
  
  return s; 
}

int[] write_w(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 15;
  int mid1y = mid; 
  int mid2x = mid1x +12;
  int mid2y = base;
  int mid3x = mid2x +12; 
  int mid3y = mid; 
  int mid4x = mid3x +12;
  int mid4y = base;
  int mid5x = mid4x +12; 
  int mid5y = mid; 
  int endx = mid5x +10;
  int endy = mid +5; 
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x -5;
  int control2y = mid1y +5; 
  
  int control3x = mid1x;
  int control3y = mid1y +10;
  int control4x = mid2x -15;
  int control4y = mid2y ; 

  int control5x = mid2x +15;
  int control5y = mid2y;  
  int control6x = mid3x;
  int control6y = mid3y +10 ; 
  
  int control7x = mid3x;
  int control7y = mid3y +10;   
  int control8x = mid4x -15; 
  int control8y = mid4y;
  
  int control9x = mid4x +15;
  int control9y = mid4y;   
  int control10x = mid5x; 
  int control10y = mid5y +10;
  
  int control11x = mid5x +4;
  int control11y = mid5y;   
  int control12x = endx -10; 
  int control12y = endy;
  
  s[0] = endx - control12x;
  s[1] = endy - control12y;
  s[2] = endx; 
  s[3] = endy; 
  
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, mid3x, mid3y, control7x, control7y, control8x, control8y, mid4x, mid4y ,control9x, control9y, control10x, control10y, mid5x, mid5y, control11x, control11y, control12x, control12y,endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, mid3x, mid3y);
  bezierVertex(control7x, control7y, control8x, control8y, mid4x, mid4y);
  bezierVertex(control9x, control9y, control10x, control10y, mid5x, mid5y);
  bezierVertex(control11x, control11y, control12x, control12y, endx, endy);
  endShape();
  
  return s; 
}

int[] write_x(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 20;
  int mid1y = mid; 
  int endx = mid1x +20;
  int endy = base; 
  
  int cross1x = endx;
  int cross1y = mid;
  int cross2x = startx +10; 
  int cross2y = base; 
  line(cross1x, cross1y, cross2x, cross2y); 
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x -10;
  int control2y = mid1y; 
  
  int control3x = mid1x +10;
  int control3y = mid1y;
  int control4x = endx -20;
  int control4y = endy; 

  s[0] = endx - control4x;
  s[1] = endy - control4y; 
  s[2] = endx; 
  s[3] = endy; 
  
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, endx, endy);
  endShape();
  
  return s; 
}

int[] write_y(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 15;
  int mid1y = mid; 
  int mid2x = mid1x +12;
  int mid2y = base;
  int mid3x = mid2x +12; 
  int mid3y = mid; 
  int mid4x = mid2x -5; 
  int mid4y = bottom; 
  int endx = mid3x +10;
  int endy = base; 
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x;
  int control2y = mid1y + 10 ; 
  
  int control3x = mid1x;
  int control3y = mid1y +10;
  int control4x = mid2x -10;
  int control4y = mid2y ; 

  int control5x = mid2x +10;
  int control5y = mid2y;  
  int control6x = mid3x;
  int control6y = mid3y +10 ; 
  
  int control7x = mid3x +3;
  int control7y = mid3y ;   
  int control8x = mid4x +10; 
  int control8y = mid4y;
  
  int control9x = mid4x -10;
  int control9y = mid4y;   
  int control10x = endx -10; 
  int control10y = endy +10;
  
  
  s[0] = endx - control10x;
  s[1] = endy - control10y;
  s[2] = endx; 
  s[3] = endy; 
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, mid3x, mid3y, control7x, control7y, control8x, control8y, mid4x, mid4y ,control9x, control9y, control10x, control10y, endx, endy};  
  s = concat(s, cArray);
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, mid3x, mid3y);
  bezierVertex(control7x, control7y, control8x, control8y, mid4x, mid4y);
  bezierVertex(control9x, control9y, control10x, control10y, endx, endy);
  endShape();
  
  return s; 
}


int[] write_z(int startx, int starty, int sx, int sy, int mid, int top, int base, int bottom, int var) {
   
  int[] s = {0,0,0,0};
  int mid1x = startx + 15;
  int mid1y = mid + int(.1*(base-mid)); 
  int mid2x = mid1x +5;
  int mid2y = base;
  int mid3x = mid2x -5; 
  int mid3y = bottom; 
  int endx = mid3x +20;
  int endy = base; 
  
  int control1x = startx + sx;
  int control1y = starty+sy; 
  int control2x = mid1x-5;
  int control2y = mid1y+5; 
  
  int control3x = mid1x +10;
  int control3y = mid1y -10;
  int control4x = mid2x +10;
  int control4y = mid2y - int(.8*(base-mid)); 

  int control5x = mid2x +30;
  int control5y = mid2y - int(1.5*(base-mid));  
  int control6x = mid3x;
  int control6y = mid3y +10 ; 
  
  int control7x = mid3x -10;
  int control7y = mid3y;   
  int control8x = endx -10; 
  int control8y = endy;
  
  s[0] = endx - control8x;
  s[1] = endy - control8y;
  s[2] = endx; 
  s[3] = endy; 
  int[] cArray = {startx, starty, control1x, control1y,control2x, control2y, mid1x, mid1y, control3x, control3y, control4x, control4y, mid2x, mid2y, control5x, control5y, control6x, control6y, mid3x,mid3y, control7x, control7y, control8x, control8y, endx, endy};  
  s = concat(s, cArray);
  
  beginShape();
  vertex(startx, starty); // first point
  bezierVertex(control1x, control1y, control2x, control2y, mid1x, mid1y);
  bezierVertex(control3x, control3y, control4x, control4y, mid2x, mid2y);
  bezierVertex(control5x, control5y, control6x, control6y, mid3x, mid3y);
  bezierVertex(control7x, control7y, control8x, control8y,endx, endy);
  endShape();
  
  return s; 
}