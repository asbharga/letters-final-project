function [Cxout, Cyout, xout, yout, difference] = CutCurves( x,y, Cx, Cy)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
complex = x+sqrt(-1)*y; 
[a,b] = size(x); 
minimums = min(complex); 
maximums = max(complex); 

figure 
for i =1:b
    for j = 1:b
        if i~=j
            [X0, Y0] = intersections(x(:,i),y(:,i),x(:,j),y(:,j));
            plot(x(:,i),y(:,i))
            hold on 
            plot(X0,Y0, 'o'); 
            
        end 
    end 
end 

difference = zeros(b,b); 
DFE = zeros(1,b); 
for i =1:b
    everyoneelse = complex;
    everyoneelse(:,i) = [];
    m = complex(:,i);
    
    for k=1:length(m)
        DFE(i) = min(min(abs(everyoneelse -m(k))));
    end 
    
    for j = 1:b
        n = complex(:,j);
        
        for k=1:length(m)
            difference(i,j) = difference(i,j) + min(abs(n-m(k))); 
        end
        
        
    end 
end 

difference = difference/length(x); 

difference(difference==0)=Inf; 
%threshold = .6*sum(min(difference))/length(min(difference));
threshold = 2;

good = zeros(1,b); 
for i = 1:b 
    for j = 1:b
        if difference(i,j) < threshold && difference(i,j) ~=0 && (difference(i,j) < difference(j,i))
            good(i)=1;      
        end
    end  
end 

rankedDFE = sort(DFE,'descend');
threshold = rankedDFE(3); 
Cxout = [];
Cyout = [];
for i = 1:b 
    if  DFE(i)>threshold || good(i) == 0  
        Cxout = [Cxout, Cx(:,i)];
        Cyout = [Cyout, Cy(:,i)];
    end 
end 
[~, l]=size(Cxout);    
figure 
xout = [];
yout = [];
for j = 1:l
    t = 0:.01:1;
    xtemp = transpose([(1-t).^3; 3*t.*(1-t).^2; 3*(1-t).*t.^2; t.^3 ]) *Cxout(:,j);
    ytemp = transpose([(1-t).^3; 3*t.*(1-t).^2; 3*(1-t).*t.^2; t.^3 ]) *Cyout(:,j);
    xout = [xout,xtemp];
    yout = [yout,ytemp]; 
    %figure 
    plot(xtemp,ytemp) 
    hold on 

     
end 


end

