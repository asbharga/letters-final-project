function [Cx, Cy] = BezierRegression2Curves( xsub, ysub, p )
%Takes in the x and y values
    % Produces control points for a degree-3 bezier curve by first
    % translating into t-values and running a linear regression where
    % Phi(t) = t.^3, t.^2, t, 1 

% Calculates t-values from x,y values 
done = 0; 
npointsgone = 0; 
olderror = 10000; 
oldCx = [];
oldCy = []; 
npoints = length(xsub);
tvecp = []; 
tspacing = [];
error = []; 
while done==0 && npoints > 3
    npoints = length(xsub) - npointsgone; 
    tspacing = sqrt((xsub(1:npoints-1)-xsub(2:npoints)).^2 + (ysub(1:npoints-1)-ysub(2:npoints)).^2); 
    total = sum(tspacing); 

    tvec = []; 
    tvec(1) = 0; 
    for i = 1:length(tspacing)
        tvec(i+1) = sum(tspacing(1:i))/total; 
    end 

    tmat = []; 
    tmat = [tvec.^3; tvec.^2; tvec; ones(1,length(tvec))]; 

    % Calculating Bezier Regression 
    M = [-1 3 -3 1; 3 -6 3 0; -3 3 0 0; 1 0 0 0]; 

    Cy = inv(M)* inv(tmat*transpose(tmat)) * tmat*transpose(ysub(1:length(tmat))); 
    Cx = inv(M)* inv(tmat*transpose(tmat)) * tmat*transpose(xsub(1:length(tmat))); 

    error = findBezierError( xsub(1:length(tmat)), ysub(1:length(tmat)), Cx, Cy, tvec );  
%     figure(4) 
%     plot(xsub(1:length(tmat)), ysub(1:length(tmat))); 
%     
    if  error < .8
        done = 1; 
    end 
    
    %update values for next iteration 
    if ~done 
        oldCx = Cx;
        oldCy = Cy;
        olderror = error; 
        npointsgone = npointsgone+1; 
    end 
end 

% put stuff back 
if ~isempty(oldCx); 
    Cx = oldCx;
    Cy = oldCy; 
end 



%% test out that regression! 
% if p>0
%     t = 0:.01:1;
% 
%     x = transpose([(1-t).^3; 3*t.*(1-t).^2; 3*(1-t).*t.^2; t.^3 ]) *Cx;
%     y = transpose([(1-t).^3; 3*t.*(1-t).^2; 3*(1-t).*t.^2; t.^3 ]) *Cy;

%     figure
%     plot(xsub, ysub, 'r*')
%     hold on 
%     plot(x,y, 'r')
% 
%     figure
%     plot(tvec,xsub(1:length(tvec)))
%     hold on
%     plot(t,x)
%     plot(tvec,ysub(1:length(tvec)), 'r')
%     plot(t,y, 'r')
% end 

    if npoints < 6 || isempty(Cx) 
        Cx = zeros(4,1);
        Cy = zeros(4,1);
    end 
    
    %don't allow if you have a huge space between points 
    if max(tspacing) > 4*sum(tspacing)/length(tspacing)
        Cx = zeros(4,1);
        Cy = zeros(4,1);
    end 
end


function [error] = findBezierError( xsub, ysub, Cx, Cy, tvec )

xerr = transpose([(1-tvec).^3; 3*tvec.*(1-tvec).^2; 3*(1-tvec).*tvec.^2; tvec.^3 ]) *Cx - xsub';
yerr = transpose([(1-tvec).^3; 3*tvec.*(1-tvec).^2; 3*(1-tvec).*tvec.^2; tvec.^3 ]) *Cy - ysub';
error = sum ( sqrt ( xerr.^2 + yerr.^2))/length(xerr); 

end 