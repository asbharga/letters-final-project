function [ x, y ] = FormatControls( Cin )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

 
xCout = [];
yCout = [];

for i = 1:length(Cin)/2 
    xCout = [xCout, Cin(i*2-1)];
    yCout = [yCout, Cin(i*2)];
end 

n = (length(xCout)-1)/3;

x = [];
y = [];

for i = 0:n-1
    x = [x, xCout(1+i*3:4+i*3)']; 
    y = [y, yCout(1+i*3:4+i*3)']; 
end 


end

