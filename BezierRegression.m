function [Cx, Cy] = BezierRegression( xsub, ysub, p )
%Takes in the x and y values
    % Produces control points for a degree-3 bezier curve by first
    % translating into t-values and running a linear regression where
    % Phi(t) = t.^3, t.^2, t, 1 

% Calculates t-values from x,y values 
npoints = length(xsub); 
tspacing = sqrt((xsub(1:npoints-1)-xsub(2:npoints)).^2 + (ysub(1:npoints-1)-ysub(2:npoints)).^2); 
total = sum(tspacing); 

tvec(1) = 0; 
for i = 1:length(tspacing)
    tvec(i+1) = sum(tspacing(1:i))/total; 
end 

tmat = [tvec.^3; tvec.^2; tvec; ones(1,length(tvec))]; 

% Calculating Bezier Regression 
M = [-1 3 -3 1; 3 -6 3 0; -3 3 0 0; 1 0 0 0]; 

Cy = inv(M)* inv(tmat*transpose(tmat)) * tmat*transpose(ysub); 
Cx = inv(M)* inv(tmat*transpose(tmat)) * tmat*transpose(xsub); 

error = findBezierError( xsub, ysub, Cx, Cy, tvec ); 

%% test out that regression! 
if p>0
    t = 0:.01:1;

    x = transpose([(1-t).^3; 3*t.*(1-t).^2; 3*(1-t).*t.^2; t.^3 ]) *Cx;
    y = transpose([(1-t).^3; 3*t.*(1-t).^2; 3*(1-t).*t.^2; t.^3 ]) *Cy;

%     figure
%     plot(xsub, ysub, 'r*')
%     hold on 
%     plot(x,y, 'r')
% 
%     figure
%     plot(tvec,xsub)
%     hold on
%     plot(t,x)
%     plot(tvec,ysub, 'r')
%     plot(t,y, 'r')

end 

    if length(xsub) < 6
        Cx = zeros(4,1);
        Cy = zeros(4,1);
    end 
end


function [error] = findBezierError( xsub, ysub, Cx, Cy, tvec )

xerr = transpose([(1-tvec).^3; 3*tvec.*(1-tvec).^2; 3*(1-tvec).*tvec.^2; tvec.^3 ]) *Cx - xsub';
yerr = transpose([(1-tvec).^3; 3*tvec.*(1-tvec).^2; 3*(1-tvec).*tvec.^2; tvec.^3 ]) *Cy - ysub';
error = sum ( sqrt ( xerr.^2 + yerr.^2))/length(xerr); 

end 