clear
%% 

%preprocessing the image 
A = importdata('q.txt');
AControls = importdata('qoutput.txt'); 

[xind_out, yind_out] = LetterProcessing(A); 
[Cx_real, Cy_real] = FormatControls(AControls); 

Cy_real = 118 - Cy_real; 
Cx_real = Cx_real -3; 


[~, n] = size(Cx_real);
x_real = [];
y_real = [];
for j = 1:n
    t = 0:.01:1;
    x = transpose([(1-t).^3; 3*t.*(1-t).^2; 3*(1-t).*t.^2; t.^3 ]) *Cx_real(:,j);
    y = transpose([(1-t).^3; 3*t.*(1-t).^2; 3*(1-t).*t.^2; t.^3 ]) *Cy_real(:,j);
    x_real = [x_real,x];
    y_real = [y_real,y]; 
    
    plot(x,y, 'g') 
    hold on 
    plot(xind_out,yind_out, '*')

end 

% initialize the subsets 

% New plan: seed and iterate! 
% randomly pick a point and then run the algorithm 
% keep a vector of Cx, Cy points 
% keep a count of how many times the Cx, Cy points show up 
% threshold the curves so that you only keep the Cx, Cy's that have a large
% number of showing-up times. 
 
%% 

NJ = 30;
for j = 1:NJ

%k = randi(length(xind_out)); 
k = floor( (j/(NJ+1)) * length(xind_out) ) +1; 
%k = length(xind_out);
%k =1; 

    xind_subset = xind_out(k);
    yind_subset = yind_out(k); 
    xind_notsubset = xind_out;
    yind_notsubset = yind_out;
    xind_notsubset(k) = []; 
    yind_notsubset(k) = []; 

%     figure
%     plot(xind_notsubset, yind_notsubset, '*');
%     hold on 
%     plot(xind_subset, yind_subset, 'r*')

    for i = 1:4
        % perform algorithm 
        [ xind_subset, yind_subset, xind_notsubset, yind_notsubset] = FindLineSeg( xind_subset, yind_subset, xind_notsubset, yind_notsubset ); 

        % Calculate t stuff  
        npoints = length(xind_subset)-1;
        [Cx(:,i+j*4), Cy(:,i+j*4)] = BezierRegression2Curves(xind_subset(1:npoints), yind_subset(1:npoints), 1); 

        
         if mod(i,2)==1
            xind_temp = xind_subset;
            yind_temp = yind_subset; 
            xind_subset = zeros(1,2);
            yind_subset = zeros(1,2); 
            xind_subset(1) = xind_temp(npoints); 
            xind_subset(2) = xind_temp(npoints-1);
            yind_subset(1) = yind_temp(npoints); 
            yind_subset(2) = yind_temp(npoints-1);
       else 
           xind_subset = xind_subset(npoints:npoints+1); 
           yind_subset = yind_subset(npoints:npoints+1); 
       end 
        xind_notsubset = xind_out; 
        yind_notsubset = yind_out;             
            

        for q = 1:2
            [b] = find(xind_notsubset == xind_subset(q));
            [c] = find(yind_notsubset == yind_subset(q));
            m = intersect(b,c); 
            xind_notsubset(m) = [];
            yind_notsubset(m) = []; 

        end 

    end 
end 


%Consolidate Cx, Cy values 
%% 

Cx_small = Cx(:,1);
Cy_small = Cy(:,1); 
l_cx =1; 
Cx_counter = 1; 
for j = 2:length(Cx) 
    n = find(ismember(Cx_small',Cx(:,j)','rows'));
    m = find(ismember(Cy_small',Cy(:,j)','rows'));
    if isempty(n) ==0 && isempty(m) ==0 && m(1)==n(1)
        Cx_counter(n) = Cx_counter(n) +1; 
    else
        l_cx = l_cx+1; 
        Cx_small(:,l_cx) = Cx(:,j); 
        Cy_small(:,l_cx) = Cy(:,j); 
        Cx_counter = [Cx_counter, 1]; 
    end 
end 

counter = CombineCurves( Cx_small, Cy_small, Cx_counter); 

Cx_final= [];
Cy_final = []; 


for j = 1:length(Cx_counter)
    if counter(j) > 0
        Cx_final = [Cx_final, Cx_small(:,j)]; 
        Cy_final = [Cy_final, Cy_small(:,j)];
    end 
    
end 

figure 
x_small = [];
y_small = [];
for j = 1:length(Cx_small) 
    t = 0:.01:1;
    x = transpose([(1-t).^3; 3*t.*(1-t).^2; 3*(1-t).*t.^2; t.^3 ]) *Cx_small(:,j);
    y = transpose([(1-t).^3; 3*t.*(1-t).^2; 3*(1-t).*t.^2; t.^3 ]) *Cy_small(:,j);
    x_small = [x_small,x];
    y_small = [y_small,y];
    %figure 
    plot(x,y) 
    hold on 
    plot(xind_out,yind_out, '*')
    title('Set of all curves output from previous section'); 
     
end 

figure 
[~, n] = size(Cx_final);
x_final = [];
y_final = [];
for j = 1:n 
    t = 0:.01:1;
    x = transpose([(1-t).^3; 3*t.*(1-t).^2; 3*(1-t).*t.^2; t.^3 ]) *Cx_final(:,j);
    y = transpose([(1-t).^3; 3*t.*(1-t).^2; 3*(1-t).*t.^2; t.^3 ]) *Cy_final(:,j);
    if sum(x)>0
        x_final = [x_final,x];
        y_final = [y_final,y]; 
    end 
    
    plot(x,y, 'r') 
    hold on 
    plot(xind_out,yind_out, '*')
    title('Set of final curves'); 

end 

%% 

[~, n] = size(Cx_real);
x_real = [];
y_real = [];
for j = 1:n
    t = 0:.01:1;
    x = transpose([(1-t).^3; 3*t.*(1-t).^2; 3*(1-t).*t.^2; t.^3 ]) *Cx_real(:,j);
    y = transpose([(1-t).^3; 3*t.*(1-t).^2; 3*(1-t).*t.^2; t.^3 ]) *Cy_real(:,j);
    x_real = [x_real,x];
    y_real = [y_real,y]; 
    
    %plot(x,y, 'g') 
    %hold on 
    %plot(xind_out,yind_out, '*')

end 


[Cxout, Cyout, xout, yout, difference] = CutCurves( x_final,y_final, Cx_final(:,2:end), Cy_final(:,2:end)); 
[ Oerror ] = CurveError( xout,yout,x_real,y_real ) 
[ Uerror ] = CurveError( x_real,y_real,xout,yout ) 
hold on 
plot(x_real, y_real, 'g'); 