clear


A = importdata('a.txt');
A = A+1; 
k=1; 
val = max(min(A));

for j = 1:40
    for i = 1:120 
        if A(i,j) < val*.8 
            A(i,j) =60; 
            xind(k) = j; 
            yind(k) = i;
            k = k+1; 
        else 
            A(i,j) = 0; 
        end 
    end 
end 

current_xind = xind(1); 
next_xind = xind(1); 
nsame = 1; 
xind_out = [];
yind_out = [];

for j = 2:length(xind); 
    next_xind = xind(j);
    if next_xind == current_xind
        nsame = nsame +1; 
    end 
    
    if next_xind ~= current_xind || abs(yind(j) -yind(j-1)) > 1 
        xind_out(length(xind_out)+1) = xind(j-1); 
        yind_out(length(xind_out)) = yind(j-1); 
        nsame = 1; 
    end 
    current_xind = xind(j); 
end 
% figure(1)
% plot(xind,yind, '*'); 
% hold on 
% plot(xind_out, yind_out(1:length(xind_out)), 'r*')

%% initialize the subsets 

k = 4; 

xind_subset = xind_out(k);
yind_subset = yind_out(k); 
xind_notsubset = xind_out;
yind_notsubset = yind_out;
xind_notsubset(k) = []; 
yind_notsubset(k) = []; 

figure
plot(xind_notsubset, yind_notsubset, '*');
hold on 
plot(xind_subset, yind_subset, 'r*')

%% 


lastres = 1000; 
done = 0; 
nPoints = length(xind); 
nDel_subset = zeros(1,length(xind_subset)); 
nDel_notsubset = zeros(1,length(xind_notsubset)); 

nIter = 10;
i =0; 
while (i < nIter && ~done)
    i=i+1; 
    
    nsub = length(xind_subset); 
    npoly = ceil(100*log(nsub)/nPoints);
    p = polyfit(xind_subset,yind_subset, npoly ); 

    % calculate metric for deleting points from the set 
    y_in = polyval(p,xind_subset);
    res_in = abs(yind_subset-y_in);
    dist_in = zeros(1,length(xind_subset)); 
    
    avgerr(i) = sum(res_in)/length(res_in); 
    value(i) = -log(length(res_in)/nPoints) + log(avgerr(i)); 
        
    for m = 1:length(xind_subset); 
        temp_xind_subset = xind_subset; 
        temp_xind_subset(m) = []; 
        temp_yind_subset = xind_subset; 
        temp_yind_subset(m) = []; 
        dist_ins = sqrt((temp_xind_subset-xind_subset(m)).^2 + (temp_yind_subset-yind_subset(m)).^2);
        dist_ins = sort(dist_ins); 
        if length(dist_ins) >=3
            dist_in(m) = sum(dist_ins(1:3))/3;
        else 
            dist_in(m) = sum(dist_ins)/length(dist_ins); 
        end 
    end 
    
    
    % delete one point from the set every time 
    for j = 1:3
        if i > 1 
        [m,p] = max(dist_in + res_in*.05); 
        xind_notsubset = [xind_notsubset, xind_subset(p)];
        yind_notsubset = [yind_notsubset, yind_subset(p)];
        nDel_notsubset = [nDel_notsubset, nDel_subset(p)+1]; 
        xind_subset(p) = [];
        yind_subset(p) = []; 
        nDel_subset(p) = []; 
        dist_in(p) = []; 
        res_in(p) = []; 
        end 
    end 
    
    p = polyfit(xind_subset,yind_subset, npoly); 

    % calculate metric for adding points 
        distance = zeros(1,length(xind_notsubset)); 
        y = polyval(p,xind_notsubset); 
        res_out = abs(yind_notsubset-y);
        for m = 1:length(xind_notsubset); 
            distances = (xind_subset-xind_notsubset(m)).^2 + (yind_subset-yind_notsubset(m)).^2;
            distances = sort(distances); 
            if length(distances) >=3
                distance(m) = sum(distances(1:3))/3;
            else 
                distance(m) = sum(distances)/length(distances); 
            end 
        end 

    % add points 
        for j = 1:8
            [r, n] = min(res_out + distance*.05);
                xind_subset = [xind_subset, xind_notsubset(n)];
                yind_subset = [yind_subset, yind_notsubset(n)];
                nDel_subset = [nDel_subset, nDel_notsubset(n)]; 
                yind_notsubset(n) = [];
                xind_notsubset(n) = [];
                nDel_notsubset(n) = []; 
                res_out(n) = []; 
                distance(n) = []; 
        end

    if (npoly > 1) && i > 2
        if (value(i) > value(i-1)) && (value(i) < 1.2)
            done = 1;
        end 
    end 
        
        
    %plot results 
    if mod(i,1)==0 || done==1
        figure(i)
        plot(xind_subset,yind_subset, 'g*');
        hold on  
        x_plot = max(min(xind_subset)-3,0):min(max(xind_subset)+3,40); 
        y_plot = polyval(p,x_plot);
        plot(x_plot,y_plot, 'r'); 
        plot(xind_notsubset, yind_notsubset, '*')
    end 
 

    
end 


figure
plot(avgerr) 
hold on 
plot(value, 'r'); 
legend('average error', 'value'); 

    