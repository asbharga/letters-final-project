function [ xind_out, yind_out ] = OldLetterProcessing( A )

A = A+1; 
k=1; 
%val = sum(min(A))/length(A);
val = max(min(A)); 

for j = 1:40
    for i = 1:120 
        if A(i,j) < val*.8
            A(i,j) =60; 
            xind(k) = j;  
            yind(k) = i;
            k = k+1; 
        else 
            A(i,j) = 0; 
        end 
    end 
end 



current_xind = xind(1); 
next_xind = xind(1); 
nsame = 1; 
xind_out = [];
yind_out = [];

for j = 2:length(xind); 
    next_xind = xind(j);
    if next_xind == current_xind
        nsame = nsame +1; 
    end 
    if isempty(yind_out) == 1 
        xind_out(length(xind_out)+1) = xind(j); 
        yind_out(length(xind_out)) = yind(j); 
    else 
        if next_xind ~= current_xind || abs(yind(j) - yind_out(max(length(yind_out),1))) > 3 %abs(yind(j)-yind(j-1)) >1  %
            xind_out(length(xind_out)+1) = xind(j); 
            yind_out(length(xind_out)) = yind(j); 
            nsame = 1; 
        end 
    end 
    current_xind = xind(j); 
end 

yind_out = max(yind_out) - yind_out; 
plot(xind,yind,'*'); 
figure
plot(xind_out,yind_out,'r*'); 

end

